package com.example.ccast

sealed class NetworkState<T> {
    object Loading : NetworkState<Nothing>()
    data class Success<T>(val data: T) : NetworkState<T>()
    data class Failure<T>(val error: String) : NetworkState<T>()
}
