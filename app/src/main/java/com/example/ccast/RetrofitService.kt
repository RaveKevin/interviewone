package com.example.ccast

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {
    // update with json
    @GET(value = "api/1")
    suspend fun getDinosaurs(@Query("q") query: String = "dinosaur"): Response<ImageResponse>
}