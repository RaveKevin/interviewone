package com.example.ccast

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageResponse(
    @get:Json(name = "image_name")
    val imageName: String,
    val results: List<String>
)
