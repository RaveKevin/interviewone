package com.example.ccast

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ccast.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ImageViewModel
    private val resultAdapter by lazy {
        ResultAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[ImageViewModel::class.java]
        binding.resultRv.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = resultAdapter
        }
        viewModel.imageResponse.observe(this) { state ->
            when (state) {
                is NetworkState.Loading -> {

                }
                is NetworkState.Success -> {
                    resultAdapter.submitList(state.data.results)
                }
                is NetworkState.Failure -> {}
            }
        }
    }
}