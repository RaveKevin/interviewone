package com.example.ccast

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

const val BASE_URL = "https://imsea.herokuapp.com/"

class RetrofitInstance {

    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    companion object {
        val dinosaurService by lazy {
            RetrofitInstance()
                .retrofit
                .create(RetrofitService::class.java)
        }
    }
}