package com.example.ccast

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ccast.databinding.RowItemImageBinding

class ResultAdapter() : RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

    private val urls = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultViewHolder {
        return ResultViewHolder(
            RowItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ResultViewHolder, position: Int) {
        holder.bind(urls[position])
    }

    override fun getItemCount(): Int {
        return urls.size
    }

    fun submitList(urls: List<String>) {
        this.urls.clear()
        this.urls.addAll(urls)
        notifyDataSetChanged()
    }

    class ResultViewHolder(
        private val binding: RowItemImageBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(url: String) {
            Glide.with(binding.root.context).load(url).into(binding.resultIv)
        }
    }
}