package com.example.ccast

import kotlinx.coroutines.flow.flow
import java.lang.Exception

object ImageRepository {

    private val dinosaurService get() = RetrofitInstance.dinosaurService

    fun getDinosaurs() = flow {
        emit(NetworkState.Loading)
        val response = try {
            val response = dinosaurService.getDinosaurs()
            if (response.isSuccessful && response.body() != null) {
                NetworkState.Success(response.body()!!)
            } else {
                NetworkState.Failure("No images")
            }
        } catch (ex: Exception) {
            NetworkState.Failure(ex.message.orEmpty())
        }
        emit(response)
    }
}