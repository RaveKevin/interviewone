package com.example.ccast

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData

class ImageViewModel : ViewModel() {
    val imageResponse = ImageRepository.getDinosaurs().asLiveData()
}